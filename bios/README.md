Image Receiver for IBM PC (BIOS)
================================

This is the X86_32 / X86_64 version of the chain loader. It can be booted from MBR, VBR, El Torito "No Emulation" CDROM (boot.bin)
or from an Option ROM (optionrom.bin). In all case, it will read the image from the serial line to the address 1M. Image can be in
ELF32, ELF64 or raw format. For ELF64 the first 1G of RAM is identity mapped, and long mode is set up. For ELF32 and raw format,
the entry point is called in protected mode. For raw images, the execution starts at 1M, for ELFs at their entry point.

Compilation
-----------

You'll need [fasm](http://flatassembler.net).
```
make
```
Then use boot.bin in a boot record, or burn optionrom.bin into an EEPROM (for vm, "qemu -option-rom optionrom.bin").
